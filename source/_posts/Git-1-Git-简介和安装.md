---
title: '[Git] 1 Git 简介和安装'
catalog: true
date: 2019-08-22 23:38:10
subtitle: Git 简介和安装
header-img: /img/git/git_bg.png
tags:
- Git
categories:
- Git
---

## 概述
- Git 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。
- Git 是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。
- Git 与常用的版本控制工具 CVS, Subversion 等不同，它采用了分布式版本库的方式，不必服务器端软件支持。

## 安装 Git
### 下载
Git 官网下载安装说明：https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git

下载地址：https://git-scm.com/download/win
![1](1.png)

### 安装
下载完之后，双击安装
![2](2.png)

选择安装目录
![3](3.png)

选择组件
![4](4.png)

开始菜单目录名设置
![5](5.png)

选择使用命令行环境
![6](6.png)

以下三步默认，直接点击下一步
![7](7.png)

![8](8.png)

![9](9.png)

安装完成
![10](10.png)

检验是否安装成功：回到电脑桌面，鼠标右击如果看到有两个 Git 单词则安装成功
![11](11.png)