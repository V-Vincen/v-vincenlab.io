---
title: '[Java8] 6 Lambda 表达式练习'
catalog: true
date: 2020-04-07 18:30:29
subtitle:
header-img: /img/java8/java8_bg.png
top: 99
tags:
- Java8
categories:
- Java8
---

先来写个基本类：
```java
public class LambdaTest {
    @Data
    class Student {
        private Integer id;
        private String name;
        private Integer age;

        public Student(Integer id, String name, Integer age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }
    }

    public List<Student> getStudents() {
        List<Student> list = new ArrayList<>();
        list.add(new Student(100, "小明", 10));
        list.add(new Student(101, "小红", 11));
        list.add(new Student(102, "小李", 12));
        list.add(new Student(103, "小王", 13));
        list.add(new Student(104, "小张", 14));
        list.add(new Student(105, "小五", 15));
        list.add(new Student(106, "小华", 16));
        return list;
    }
}
```

## `sorted()` 四种排序
```java
    @Test
    public void sortedTest() {
        List<Student> students = this.getStudents();
        //正序：常规写法
        List<Student> collect = students.stream().sorted((a1, a2) -> Integer.compare(a1.getAge(), a2.getAge())).collect(Collectors.toList());
        collect.forEach(System.out::println);
        System.out.println();
        //正序：lambda 简写
        List<Student> collectComparator = students.stream().sorted(Comparator.comparing(Student::getAge)).collect(Collectors.toList());
        collectComparator.forEach(System.out::println);
        System.out.println();

        //倒序：常规写法
        List<Student> collectReversed = students.stream().sorted((a1, a2) -> Integer.compare(a2.getAge(), a1.getAge())).collect(Collectors.toList());
        collectReversed.forEach(System.out::println);
        System.out.println();
        //正序：lambda 简写
        List<Student> collectComparatorReversed = students.stream().sorted(Comparator.comparing(Student::getAge).reversed()).collect(Collectors.toList());
        collectComparatorReversed.forEach(System.out::println);
    }
```

## `List` 转 `Map`
```java
    @Test
    public void listToMapTest() {
        List<Student> students = this.getStudents();
        Map<Integer, Student> collect = students.stream().collect(Collectors.toMap(Student::getId, s -> s, (s1, s2) -> s1));
        collect.keySet().forEach(key -> System.out.println(String.format("key：%s，value：%s", key, collect.get(key))));
    }
```

## `map` 映射
```java
    @Test
    public void mapTest() {
        List<Student> students = this.getStudents();
//        List<Student> students = Lists.newArrayList(new Student(null, "小炎姬", 5));
        Integer id = students.stream().map(Student::getId).max(Integer::compareTo).orElse(0);
        System.out.println(id);
    }
```

## 对象中 `String` 类型属性为空的字段赋值为 `null`
```java
public static <T> void stringEmptyToNull(T t) {
    Class<?> clazz = t.getClass();

    Field[] fields = clazz.getDeclaredFields();
    Arrays.stream(fields)
        .filter(f -> f.getType() == String.class)
        .filter(f -> {
            try {
                f.setAccessible(true);
                String value = (String) f.get(t);
                return StringUtils.isEmpty(value);
            } catch (Exception ignore) {
                return false;
            }
        })
        .forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(t, null);
                } catch (Exception ignore) {
            }
        });
}
```

