---
title: '[Git] 2 Git 的一般工作流程'
catalog: true
date: 2019-08-22 23:58:01
subtitle: Git 的一般工作流程
header-img: /img/git/git_bg.png
tags:
- Git
categories:
- Git
---

## 工作流程
- 克隆 Git 资源作为工作目录。
- 在克隆的资源上添加或修改文件。
- 如果其他人修改了，你可以更新资源。
- 在提交前查看修改。
- 提交修改。
- 在修改完成后，如果发现错误，可以撤回提交并再次修改并提交。

![1](1.png)