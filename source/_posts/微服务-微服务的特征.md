---
title: '[微服务] 微服务的特征'
catalog: true
date: 2019-07-01 19:57:51
subtitle: 微服务的具体特征
header-img: /img/microservices/microservices_bg.png
tags:
- 微服务
categories:
- 微服务
---

## 官方的定义
- 一系列的独立的服务共同组成系统
- 单独部署，跑在自己的进程中
- 每个服务为独立的业务开发
- 分布式管理
- 非常强调隔离性

## 大概的标准
- 分布式服务组成的系统
- 按照业务，而不是技术来划分组织
- 做有生命的产品而不是项目
- 强服务个体和弱通信（ Smart endpoints and dumb pipes ）
- 自动化运维（ DevOps ）
- 高度容错性
- 快速演化和迭代