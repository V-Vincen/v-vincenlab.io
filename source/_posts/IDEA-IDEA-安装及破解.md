---
title: '[IDEA] IDEA 安装及破解'
catalog: true
date: 2019-06-12 09:19:24
subtitle: 以 ideaIU-2019.1.1 版本为例
header-img: /img/idea/idea_bg.png
tags: 
- IDEA
categories:
- IDEA
---

## 在官网下载
---
https://www.jetbrains.com/idea/download/#section=windows

这里以 ideaIU-2019.1.1.exe 为例：

![图片1](1.png)
![图片2](2.png)



## 下载破解.jar
---
大神地址：https://zhile.io/ （同时有 Mybatis 插件 MybatisCodeHelperNew-2.5 和 2.6 可下载）
破解方法:

- 先下载压缩包解压后得到 jetbrains-agent.jar，把它放到你认为合适的文件夹内。如果你没有 gitee 账号，你也可以在这里下载：https://zhile.io/2018/08/17/jetbrains-license-server-crack.html
- 启动你的 IDE，如果上来就需要注册，选择：试用（Evaluate for free）进入 IDE
- 点击你要注册的 IDE 菜单：" Configure " 或 " Help " -> " Edit Custom VM Options ... "，如果提示是否要创建文件，请点 "是|Yes" 。参考文章：https://intellij-support.jetbrains.com/hc/en-us/articles/206544869
- 在打开的 vmoptions 编辑窗口末行添加："-javaagent:/absolute/path/to/jetbrains-agent.jar"，一定要自己确认好路径，填错会导致IDE打不开！！！最好使用绝对路径。

> 示例:
>
> mac: -javaagent:/Users/neo/jetbrains-agent.jar
>
> linux: -javaagent:/home/neo/jetbrains-agent.jar
>
> windows: -javaagent:C:\Users\neo\jetbrains-agent.jar

- 如果还是填错了，参考这篇文章编辑 vmoptions 补救：https://intellij-support.jetbrains.com/hc/en-us/articles/206544519
- 重启你的IDE。
- 点击IDE菜单 " Help " -> " Register... " 或 " Configure " -> " Manage License... "，支持两种注册方式：License server 和 Activation code:
  - 选择 License server 方式，地址填入：http://jetbrains-license-server （应该会自动填上），或者点击按钮：" Discover Server "来自动填充地址
  - 选择 Activation code 方式离线激活，请使用：ACTIVATION_CODE.txt 内的注册码激活如果注册窗口一直弹出，请去 hosts 文件里移除 jetbrains 相关的项目。如果你需要自定义 License name，请访问：https://zhile.io/custom-license.html



## IDEA 初始化参数（idea64.exe.vmoptions）
---
编写 idea 安装目录 bin 文件夹下的 idea64.exe.vmoptions：

```
-Xms512m
-Xmx1500m
-XX:ReservedCodeCacheSize=480m
-XX:+UseConcMarkSweepGC
-XX:SoftRefLRUPolicyMSPerMB=50
-ea
-Dsun.io.useCanonCaches=false
-Djava.net.preferIPv4Stack=true
-Djdk.http.auth.tunneling.disabledSchemes=""
-XX:+HeapDumpOnOutOfMemoryError
-XX:-OmitStackTraceInFastThrow
-Dfile.encoding=UTF-8  # 防止idea控制台出现乱码的配置

-javaagent:E:\IDEA\IntelliJ IDEA 2019.1.1\bin\jetbrains-agent.jar
```



