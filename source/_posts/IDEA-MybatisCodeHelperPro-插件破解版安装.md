---
title: '[IDEA] MybatisCodeHelperPro 插件破解版安装'
catalog: true
date: 2019-06-14 01:28:35
subtitle: IDEA Mybatis 插件安装及使用
header-img: /img/idea/idea_bg2.png
tags:
- IDEA
categories:
- IDEA
---

MybatisCodeHelperPro（V2.5） 插件使用视频：<https://www.bilibili.com/video/av23458308/>



## 安装

Git地址：https://github.com/pengzhile/MyBatisCodeHelper-Pro-Crack/releases（已被删）

编写大神博客地址：<https://zhile.io/> （下载同时有 2019.1.1 版本的 IDE 破解版）

下载最新版（当前是 V2.5）的 zip 压缩包到本机，然后，IDEA 里面打开 plugin 安装界面（File -> Settings -> plugin），选择从本地硬盘路径安装：

![1](1.png)



![2](2.png)

安装完成重启IDEA，然后界面底部会有个提示让你输入key激活MybatisCodeHelperPro插件，你随便输入内容便可成功激活。



## 使用

激活后，来看看最常用的 DAO 跟 mapper.xml 之间的跳转，点击接口左边的蓝色箭头就跳转到对应的 xml 位置了：

![3](3.png)

![4](4.png)

如果mapper.xml 中没有实现对应的 sql，可通过快捷键自动生成：

![5](5.png)

具体功能：

![6](6.png)

官方文档在这：<https://gejun123456.github.io/MyBatisCodeHelper-Pro/#/>

![7](7.png)



![8](8.png)

