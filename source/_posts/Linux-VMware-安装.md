---
title: '[Linux] VMware 安装'
catalog: true
date: 2019-07-14 01:23:37
subtitle:
header-img: /img/linux/vmware_bg.png
tags:
- Linux
categories:
- Linux
---

## VMware 简介
VMWare （Virtual Machine ware）是一个“虚拟 PC ”软件公司.它的产品可以使你在一台机器上同时运行二个或更多 Windows、DOS、LINUX 系统。与“多启动”系统相比，VMWare 采用了完全不同的概念。

多启动系统在一个时刻只能运行一个系统，在系统切换时需要重新启动机器。VMWare 是真正“同时”运行，多个操作系统在主系统的平台上，就象标准 Windows 应用程序那样切换。而且每个操作系统你都可以进行虚拟的分区、配置而不影响真实硬盘的数据，你甚至可以通过网卡将几台虚拟机用网卡连接为一个局域网，极其方便。

安装在 VMware 操作系统性能上比直接安装在硬盘上的系统低不少，因此，比较适合学习和测试。

## VMware 下载
[官网下载地址](https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/15_0?wd=&eqid=f3663a16000ffcd8000000025d2a0fa0)

## VMware 安装（以 VMware 14 为例）
双击安装包，进入安装页面
![1](1.png)
![2](2.png)
![3](3.png)
![4](4.png)
![5](5.png)
![6](6.png)
![7](7.png)
![8](8.png)