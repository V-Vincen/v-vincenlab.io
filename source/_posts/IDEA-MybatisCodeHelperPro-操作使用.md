---
title: '[IDEA] MybatisCodeHelperPro 操作使用'
catalog: true
date: 2019-06-14 01:58:52
subtitle: Mybatis 插件操作使用
header-img: /img/idea/idea_bg2.png
tags:
- IDEA
categories:
- IDEA
---

MybatisCodeHelperPro（V2.6） 插件使用视频：<https://www.bilibili.com/video/av50632948>

支持 mysql + oracle + sqlite



## 具体操作使用

根据 java 对象一键生成 Dao 接口、Service接口、Xml、数据库建表 Sql 文件，提供 dao 与 xml 的跳转，支持创建多字段索引、多字段唯一约束

![1](1.gif)



根据 mapper 中方法名直接生成 sql 并补全方法（可生成大部分单表操作的 sql ）

![2](2.gif)



支持生成 if - test 语句

![3](3.gif)



支持使用 mybatis - generator

![4](4.gif)

另外还有 mybatis 接口方法名重构支持，property refid resultMap 等的自动补全 mybatis 中 sql 自动补全等功能



再来一张通过方法名直接生成 sql 

![5](5.gif)

注意这个是收费版一年 29 元 ，地址： <https://github.com/gejun123456/MyBatisCodeHelper-Pro>